# Trello board

## Copy project

```
git clone https://gitlab.com/Kotsur/calendar-frontend.git
```

## Create MySql database

```
 CREATE DATABASE trello;
```

## Change [application.properties](src%2Fmain%2Fresources%2Fapplication.properties)

```
spring.datasource.username=login
spring.datasource.password=password
```

## Project setup

```
mvn clean install
```

## Project start

```
mvn spring-boot:run
```

## Endpoints columns

### Get all column

```
GET http://localhost:8080/columns
```

### Create column request

```
POST http://localhost:8080/columns
{
    "name": "test 2 column"
}
```

### Create column response

```
{
    "id": 3,
    "name": "test 2 column",
    "orderField": 2,
    "tasksList": []
}
```

### Update column request

```
PUT http://localhost:8080/columns
{
  "idColumn": 1,
  "name": "new name 6"
}
```

### Update column response

```
{
    "id": 2,
    "name": "new name 6",
    "orderField": 0,
    "tasksList": [
        {
            "id": 7,
            "localDateTime": "2023-11-27T10:24:19.169912",
            "name": "task name 2",
            "descr": "task descr",
            "orderField": 12,
            "idColumn": 2
        },
        {
            "id": 8,
            "localDateTime": "2023-11-27T10:24:20.242065",
            "name": "task name 2",
            "descr": "task descr",
            "orderField": 1,
            "idColumn": 2
        }
    ]
}
```

### Change column order request
```
PATCH http://localhost:8080/columns/change-order
{
   "idColumn": 2,
   "orderField": 0
}
```
### Change column order response
```
{
    "id": 2,
    "name": "new name 6",
    "orderField": 0,
    "tasksList": [
        {
            "id": 7,
            "localDateTime": "2023-11-27T10:24:19.169912",
            "name": "task name 2",
            "descr": "task descr",
            "orderField": 12,
            "idColumn": 2
        },
        {
            "id": 8,
            "localDateTime": "2023-11-27T10:24:20.242065",
            "name": "task name 2",
            "descr": "task descr",
            "orderField": 1,
            "idColumn": 2
        }
    ]
}
```
### Delete column request
```
DELETE http://localhost:8080/columns
{
   "idColumn": 1
}
```
### Delete column response
```
{
    "idColumn": 1,
    "name": "new name 6"
}
```

## Endpoints tasks
### Create task request
```
POST http://localhost:8080/tasks
{
    "name":"task name 2",
    "descr": "task descr",
    "idColumn": 2
}
```
### Create task response
```
{
    "id": 8,
    "localDateTime": "2023-11-27T10:24:20.242064958",
    "name": "task name 2",
    "descr": "task descr",
    "orderField": 0,
    "idColumn": 2
}
```
### Update task request
```
PUT http://localhost:8080/tasks
{
    "name":"task new name",
    "descr": "task descr 66",
    "idTask": 8
}
```
### Update task response
```
{
    "id": 8,
    "localDateTime": "2023-11-27T10:24:20.242065",
    "name": "task new name",
    "descr": "task descr 66",
    "orderField": 1,
    "idColumn": 2
}
```
### Delete task request
```
DELETE http://localhost:8080/tasks
{
    "idTask":8
}
```
### Delete task response
```
{
    "idTask": 8,
    "name": "task new name"
}
```
### Change column task request
```
PATCH http://localhost:8080/tasks/change-column
{
    "idTask":7,
    "idColumn":2
}
```
### Change column task response
```
{
    "id": 7,
    "localDateTime": "2023-11-27T10:24:19.169912",
    "name": "task name 2",
    "descr": "task descr",
    "orderField": 2,
    "idColumn": 2
}
```
### Change order task request
```
PATCH http://localhost:8080/tasks/change-order
{
    "idTask":12,
    "orderField":0
}
```
### Change order task response
```
{
    "id": 12,
    "localDateTime": "2023-11-27T11:20:35.474307",
    "name": "task name 2",
    "descr": "task descr",
    "orderField": 0,
    "idColumn": 2
}
```