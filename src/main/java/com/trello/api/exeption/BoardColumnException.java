package com.trello.api.exeption;

public class BoardColumnException extends RuntimeException{
    public BoardColumnException(String message) {
        super(message);
    }
}
