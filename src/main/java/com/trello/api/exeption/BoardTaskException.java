package com.trello.api.exeption;

public class BoardTaskException extends RuntimeException{
    public BoardTaskException(String message) {
        super(message);
    }
}
