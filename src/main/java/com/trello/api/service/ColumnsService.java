package com.trello.api.service;

import com.trello.api.payload.request.BoardColumnCreateRequest;
import com.trello.api.payload.request.BoardColumnDeleteRequest;
import com.trello.api.payload.request.BoardColumnOrderRequest;
import com.trello.api.payload.request.BoardColumnUpdateRequest;
import com.trello.api.payload.response.BoardColumnResponse;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ColumnsService {

    ResponseEntity<List<BoardColumnResponse>> getAllColumns();
    ResponseEntity<Object> createColumn(BoardColumnCreateRequest columnRequest);
    ResponseEntity<Object> updateColumn(BoardColumnUpdateRequest boardColumnUpdateRequest);
    ResponseEntity<Object> changeOrderColumn(BoardColumnOrderRequest columnOrderRequest);
    ResponseEntity<Object> deleteColumn(BoardColumnDeleteRequest boardColumnDeleteRequest);



}
