package com.trello.api.service;

import com.trello.api.exeption.BoardColumnException;
import com.trello.api.model.BoardColumn;
import com.trello.api.model.BoardTask;
import com.trello.api.payload.request.BoardColumnCreateRequest;
import com.trello.api.payload.request.BoardColumnDeleteRequest;
import com.trello.api.payload.request.BoardColumnOrderRequest;
import com.trello.api.payload.request.BoardColumnUpdateRequest;
import com.trello.api.payload.response.BoardColmnDeleteResponse;
import com.trello.api.payload.response.BoardColumnResponse;
import com.trello.api.payload.response.BoardTaskResponse;
import com.trello.api.payload.response.ErrorBoardColumnResponse;
import com.trello.api.repository.BoardColumnRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class ColumnsServiceImpl implements ColumnsService {

    private final BoardColumnRepository boardColumnRepository;

    public ColumnsServiceImpl(BoardColumnRepository boardColumnRepository) {
        this.boardColumnRepository = boardColumnRepository;

    }

    @Override
    public ResponseEntity<List<BoardColumnResponse>> getAllColumns() {
        List<BoardColumn> boardColumnList = boardColumnRepository.findAllByOrderByOrderFieldAsc();
        List<BoardColumnResponse> boardColumnResponses = boardColumnResponses(boardColumnList);
        return ResponseEntity.ok(boardColumnResponses);
    }

    @Override
    public ResponseEntity<Object> createColumn(BoardColumnCreateRequest columnRequest) {
        BoardColumn boardColumn = new BoardColumn(columnRequest.getName(), findMaxOrderField());
        try {
            boardColumn = boardColumnRepository.save(boardColumn);
            return ResponseEntity.ok(new BoardColumnResponse(boardColumn.getId(), boardColumn.getName(), boardColumn.getOrderField(), boardTaskResponses(boardColumn.getTasksList())));
        } catch (Exception e) {
            ErrorBoardColumnResponse errorBoardColumnResponce = new ErrorBoardColumnResponse(new BoardColumnException("Error save column"));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorBoardColumnResponce.getErrorMap());
        }
    }

    @Override
    public ResponseEntity<Object> updateColumn(BoardColumnUpdateRequest boardColumnUpdateRequest) {
        try {
            BoardColumn boardColumn = boardColumnRepository.findByIdFetchTasks(boardColumnUpdateRequest.getIdColumn()).orElseThrow(() -> new BoardColumnException("Column not found"));
            boardColumn.setName(boardColumnUpdateRequest.getName());
            boardColumnRepository.save(boardColumn);
            return ResponseEntity.ok(new BoardColumnResponse(boardColumn.getId(), boardColumn.getName(), boardColumn.getOrderField(), boardTaskResponses(boardColumn.getTasksList())));
        } catch (BoardColumnException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorBoardColumnResponse(e).getErrorMap());
        }
    }

    @Override
    public ResponseEntity<Object> changeOrderColumn(BoardColumnOrderRequest columnOrderRequest) {
        try {
            BoardColumn boardColumn = boardColumnRepository.findByIdFetchTasks(columnOrderRequest.getIdColumn()).orElseThrow(() -> new BoardColumnException("Column not found"));
            updateOrder(columnOrderRequest.getOrderField(), boardColumn);
            return ResponseEntity.ok(new BoardColumnResponse(boardColumn.getId(), boardColumn.getName(), boardColumn.getOrderField(), boardTaskResponses(boardColumn.getTasksList())));
        } catch (BoardColumnException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorBoardColumnResponse(e).getErrorMap());
        }catch (Exception e){
            ErrorBoardColumnResponse errorBoardColumnResponce = new ErrorBoardColumnResponse(new BoardColumnException("Error save column"));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorBoardColumnResponce.getErrorMap());
        }
    }

    @Override
    public ResponseEntity<Object> deleteColumn(BoardColumnDeleteRequest boardColumnDeleteRequest) {
        try {
            BoardColumn boardColumn = boardColumnRepository.findByIdFetchTasks(boardColumnDeleteRequest.getIdColumn()).orElseThrow(() -> new BoardColumnException("Column not found"));
            BoardColmnDeleteResponse boardColmnDeleteResponse = new BoardColmnDeleteResponse(boardColumn.getId(), boardColumn.getName());
            boardColumnRepository.delete(boardColumn);
            updateOrder(null, boardColumn);
            return ResponseEntity.ok(boardColmnDeleteResponse);
        } catch (BoardColumnException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorBoardColumnResponse(e).getErrorMap());
        }
    }

    private Integer findMaxOrderField() {
        Integer maxOrderField = boardColumnRepository.findMaxOrder();
        if (maxOrderField == null) {
            return maxOrderField = 0;
        }
        return maxOrderField + 1;
    }
    private List<BoardColumnResponse> boardColumnResponses(List<BoardColumn> boardColumnList) {
        List<BoardColumnResponse> boardColumnResponses = new ArrayList<>();
        for (BoardColumn boardColumn : boardColumnList) {
            boardColumnResponses.add(new BoardColumnResponse(boardColumn.getId(), boardColumn.getName(), boardColumn.getOrderField(), boardTaskResponses(boardColumn.getTasksList())));
        }
        return boardColumnResponses;
    }

    private List<BoardTaskResponse> boardTaskResponses(List<BoardTask> boardTasks) {
        List<BoardTaskResponse> boardTaskResponses = new ArrayList<>();
        for (BoardTask boardTask : boardTasks) {
            boardTaskResponses.add(
                    new BoardTaskResponse(
                            boardTask.getId(),
                            boardTask.getLocalDateTime(),
                            boardTask.getName(),
                            boardTask.getDescr(),
                            boardTask.getOrderField(),
                            boardTask.getBoardColumn().getId()));
        }
        return boardTaskResponses;
    }

    private List<BoardColumn> updateOrder(Integer orderField, BoardColumn boardColumn){
        List<BoardColumn> boardColumnArrayList = boardColumnRepository.findAllByOrderByOrderFieldAsc();
        boardColumnArrayList.removeIf(b ->b.getId().equals(boardColumn.getId()));
        if(orderField!=null) {
            boardColumnArrayList.add(orderField, boardColumn);
        }
        Integer order = 0;
        for (BoardColumn column : boardColumnArrayList) {
            column.setOrderField(order);
            order++;
        }
        boardColumnRepository.saveAll(boardColumnArrayList);
        return boardColumnArrayList;
    }
}
