package com.trello.api.service;

import com.trello.api.exeption.BoardColumnException;
import com.trello.api.exeption.BoardTaskException;
import com.trello.api.model.BoardColumn;
import com.trello.api.model.BoardTask;
import com.trello.api.payload.request.*;
import com.trello.api.payload.response.BoardTaskDeleteResponse;
import com.trello.api.payload.response.BoardTaskResponse;
import com.trello.api.payload.response.ErrorTaskResponse;
import com.trello.api.repository.BoardColumnRepository;
import com.trello.api.repository.BoardTaskRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

    private final BoardTaskRepository boardTaskRepository;
    private final BoardColumnRepository boardColumnRepository;

    public TaskServiceImpl(BoardTaskRepository boardTaskRepository, BoardColumnRepository boardColumnRepository) {
        this.boardTaskRepository = boardTaskRepository;
        this.boardColumnRepository = boardColumnRepository;
    }

    @Override
    public ResponseEntity<Object> createTask(BoardTaskCreateRequest taskRequest) {
        try {
            BoardColumn boardColumn = boardColumnRepository.findById(taskRequest.getIdColumn()).orElseThrow(() -> new BoardTaskException("Column not found"));
            BoardTask boardTask = new BoardTask(taskRequest.getName(), taskRequest.getDescr(), findMaxOrderField(boardColumn), boardColumn);
            boardTaskRepository.save(boardTask);
            return ResponseEntity.ok(
                    new BoardTaskResponse(
                            boardTask.getId(),
                            boardTask.getLocalDateTime(),
                            boardTask.getName(),
                            boardTask.getDescr(),
                            boardTask.getOrderField(),
                            boardColumn.getId()));
        } catch (BoardTaskException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorTaskResponse(e).getErrorMap());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorTaskResponse(new BoardTaskException("Error save task")).getErrorMap());
        }

    }

    @Override
    public ResponseEntity<Object> updateTask(BoardTaskUpdateRequest taskRequest) {
        try {
            BoardTask boardTask = boardTaskRepository.findById(taskRequest.getIdTask()).orElseThrow(() -> new BoardTaskException("Task not found"));
            boardTask.setName(taskRequest.getName());
            boardTask.setDescr(taskRequest.getDescr());
            boardTaskRepository.save(boardTask);
            return ResponseEntity.ok(
                    new BoardTaskResponse(
                            boardTask.getId(),
                            boardTask.getLocalDateTime(),
                            boardTask.getName(),
                            boardTask.getDescr(),
                            boardTask.getOrderField(),
                            boardTask.getBoardColumn().getId()));
        } catch (BoardTaskException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorTaskResponse(e).getErrorMap());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorTaskResponse(new BoardTaskException("Error save task")).getErrorMap());
        }
    }

    @Override
    public ResponseEntity<Object> deleteTask(BoardTaskDeleteRequest boardTaskDeleteRequest) {
        try {
            BoardTask boardTask = boardTaskRepository.findById(boardTaskDeleteRequest.getIdTask()).orElseThrow(() -> new BoardTaskException("Task not found"));
            boardTaskRepository.delete(boardTask);
            updateOrder(null, boardTask.getBoardColumn(), boardTask);
            return ResponseEntity.ok(
                    new BoardTaskDeleteResponse(
                            boardTask.getId(),
                            boardTask.getName()));
        } catch (BoardTaskException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorTaskResponse(e).getErrorMap());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorTaskResponse(new BoardTaskException("Error save task")).getErrorMap());
        }
    }

    @Override
    public ResponseEntity<Object> changeColumnTask(BoardTaskChangeColumnRequest boardTaskChangeColumnRequest) {
        try {
            BoardColumn boardColumn = boardColumnRepository.findById(boardTaskChangeColumnRequest.getIdColumn()).orElseThrow(() -> new BoardColumnException("Column not found"));
            BoardTask boardTask = boardTaskRepository.findById(boardTaskChangeColumnRequest.getIdTask()).orElseThrow(() -> new BoardTaskException("Task not found"));
            boardTask.setOrderField(findMaxOrderField(boardColumn));
            boardTask.setBoardColumn(boardColumn);
            boardTaskRepository.save(boardTask);
            updateOrder(null, boardColumn, boardTask);
            return ResponseEntity.ok(
                    new BoardTaskResponse(
                            boardTask.getId(),
                            boardTask.getLocalDateTime(),
                            boardTask.getName(),
                            boardTask.getDescr(),
                            boardTask.getOrderField(),
                            boardTask.getBoardColumn().getId()));
        } catch (BoardColumnException | BoardTaskException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorTaskResponse(e).getErrorMap());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorTaskResponse(new BoardTaskException("Error save task")).getErrorMap());
        }
    }

    @Override
    public ResponseEntity<Object> changeOrderTask(BoardTaskOrderRequest taskOrderRequest) {
        try {
            BoardTask boardTask = boardTaskRepository.findById(taskOrderRequest.getIdTask()).orElseThrow(() -> new BoardTaskException("Task not found"));
            boardTask.setOrderField(taskOrderRequest.getOrderField());
            boardTaskRepository.save(boardTask);
            updateOrder(taskOrderRequest.getOrderField(), boardTask.getBoardColumn(), boardTask);
            return ResponseEntity.ok(
                    new BoardTaskResponse(
                            boardTask.getId(),
                            boardTask.getLocalDateTime(),
                            boardTask.getName(),
                            boardTask.getDescr(),
                            boardTask.getOrderField(),
                            boardTask.getBoardColumn().getId()));
        } catch (BoardTaskException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorTaskResponse(e).getErrorMap());
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorTaskResponse(new BoardTaskException("Error change order task")).getErrorMap());
        }
    }
    private List<BoardTask> updateOrder(Integer orderField, BoardColumn boardColumn, BoardTask boardTask){
        List<BoardTask> boardTasks = boardTaskRepository.findAllByBoardColumnOrderByOrderFieldAsc(boardColumn);
        boardTasks.removeIf(b ->b.getId().equals(boardTask.getId()));
        if(orderField!=null) {
            boardTasks.add(orderField, boardTask);
        }
        Integer order = 0;
        for (BoardTask column : boardTasks) {
            column.setOrderField(order);
            order++;
        }
        boardTaskRepository.saveAll(boardTasks);
        return boardTasks;
    }

    private Integer findMaxOrderField(BoardColumn boardColumn) {
        Integer maxOrderField = boardTaskRepository.findMaxOrder(boardColumn.getId());
        if(maxOrderField==null){
            return maxOrderField = 0;
        }
        return maxOrderField+1;
    }
}
