package com.trello.api.service;

import com.trello.api.payload.request.*;
import org.springframework.http.ResponseEntity;

public interface TaskService {
    ResponseEntity<Object> createTask(BoardTaskCreateRequest taskRequest);
    ResponseEntity<Object> updateTask(BoardTaskUpdateRequest taskRequest);
    ResponseEntity<Object> deleteTask(BoardTaskDeleteRequest boardTaskDeleteRequest);
    ResponseEntity<Object> changeColumnTask(BoardTaskChangeColumnRequest boardTaskChangeColumnRequest);
    ResponseEntity<Object> changeOrderTask(BoardTaskOrderRequest taskOrderRequest);



}
