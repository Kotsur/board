package com.trello.api.repository;

import com.trello.api.model.BoardColumn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface BoardColumnRepository extends JpaRepository<BoardColumn, Long> {
    @Query("select b from BoardColumn b left join fetch b.tasksList t order by b.orderField asc, t.orderField asc")
    List<BoardColumn> findAllByOrderByOrderFieldAsc();

    @Query("select b from BoardColumn b left join fetch b.tasksList where b.id=:idBoard ")
    Optional<BoardColumn> findByIdFetchTasks(Long idBoard);
    @Query("select max(b.orderField) from BoardColumn b")
    Integer findMaxOrder();
}
