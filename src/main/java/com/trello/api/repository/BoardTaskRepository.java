package com.trello.api.repository;

import com.trello.api.model.BoardColumn;
import com.trello.api.model.BoardTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BoardTaskRepository extends JpaRepository<BoardTask, Long> {
    List<BoardTask> findAllByBoardColumnOrderByOrderFieldAsc(BoardColumn boardColumn);

    @Query("select max(b.orderField) from BoardTask b where b.boardColumn.id=:idColumn")
    Integer findMaxOrder(Long idColumn);
}
