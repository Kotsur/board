package com.trello.api.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@Table(name = "tasks")
public class BoardTask {
    public BoardTask(String name, String descr, Integer orderField, BoardColumn boardColumn) {
        this.name = name;
        this.descr = descr;
        this.orderField = orderField;
        this.boardColumn = boardColumn;
        this.localDateTime = LocalDateTime.now();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDateTime localDateTime;
    private String name;
    @Column(columnDefinition = "TEXT")
    private String descr;
    private Integer orderField;

    @ManyToOne
    @JoinColumn(name = "column_id")
    private BoardColumn boardColumn;

}
