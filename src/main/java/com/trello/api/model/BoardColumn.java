package com.trello.api.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@Table(name = "columns")
public class BoardColumn {
    public BoardColumn(String name, Integer orderField) {
        this.name = name;
        this.orderField = orderField;
    }

    public BoardColumn(Long id, String name, Integer orderField, List<BoardTask> tasksList) {
        this.id = id;
        this.name = name;
        this.orderField = orderField;
        this.tasksList = tasksList;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Integer orderField;

    @OneToMany(mappedBy = "boardColumn", cascade = CascadeType.REMOVE)
    private List<BoardTask> tasksList = new ArrayList<>();
}
