package com.trello.api.controller;

import com.trello.api.payload.request.*;
import com.trello.api.service.TaskService;
import com.trello.api.validator.ResponseErrorValidation;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/tasks")
public class TasksController {
    private final TaskService taskService;
    private final ResponseErrorValidation responseErrorValidation;
    public TasksController(TaskService taskService, ResponseErrorValidation responseErrorValidation) {
        this.taskService = taskService;
        this.responseErrorValidation = responseErrorValidation;
    }

    @PostMapping
    public ResponseEntity<Object> createTask(@Valid @RequestBody BoardTaskCreateRequest boardColumnCreateRequest, BindingResult bindingResult){
        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if(!ObjectUtils.isEmpty(errors)){
            return errors;
        }
        return taskService.createTask(boardColumnCreateRequest);
    }
    @PutMapping
    public ResponseEntity<Object> updateTask(@Valid @RequestBody BoardTaskUpdateRequest boardTaskUpdateRequest, BindingResult bindingResult){
        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if(!ObjectUtils.isEmpty(errors)){
            return errors;
        }
        return taskService.updateTask(boardTaskUpdateRequest);
    }
    @DeleteMapping
    public ResponseEntity<Object> deleteTask(@Valid @RequestBody BoardTaskDeleteRequest boardTaskDeleteRequest, BindingResult bindingResult){
        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if(!ObjectUtils.isEmpty(errors)){
            return errors;
        }
        return taskService.deleteTask(boardTaskDeleteRequest);
    }
    @PatchMapping("/change-column")
    public ResponseEntity<Object> changeColumn(@Valid @RequestBody BoardTaskChangeColumnRequest boardTaskChangeColumnRequest, BindingResult bindingResult){
        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if(!ObjectUtils.isEmpty(errors)){
            return errors;
        }
        return taskService.changeColumnTask(boardTaskChangeColumnRequest);
    }

    @PatchMapping("/change-order")
    public ResponseEntity<Object> changeOrder(@Valid @RequestBody BoardTaskOrderRequest boardTaskOrderRequest, BindingResult bindingResult){
        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if(!ObjectUtils.isEmpty(errors)){
            return errors;
        }
        return taskService.changeOrderTask(boardTaskOrderRequest);
    }
}
