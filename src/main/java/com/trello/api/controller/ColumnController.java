package com.trello.api.controller;

import com.trello.api.payload.request.BoardColumnCreateRequest;
import com.trello.api.payload.request.BoardColumnDeleteRequest;
import com.trello.api.payload.request.BoardColumnOrderRequest;
import com.trello.api.payload.request.BoardColumnUpdateRequest;
import com.trello.api.payload.response.BoardColumnResponse;
import com.trello.api.service.ColumnsService;
import com.trello.api.validator.ResponseErrorValidation;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/columns")
public class ColumnController {
    private final ColumnsService columnsService;
    private final ResponseErrorValidation responseErrorValidation;

    public ColumnController(ColumnsService columnsService, ResponseErrorValidation responseErrorValidation) {
        this.columnsService = columnsService;
        this.responseErrorValidation = responseErrorValidation;
    }


    @GetMapping
    public ResponseEntity<List<BoardColumnResponse>> getColumns(){
        return columnsService.getAllColumns();
    }

    @PostMapping
    public ResponseEntity<Object> createColumn(@Valid @RequestBody BoardColumnCreateRequest boardColumnCreateRequest, BindingResult bindingResult){
        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if(!ObjectUtils.isEmpty(errors)){
            return errors;
        }
        return columnsService.createColumn(boardColumnCreateRequest);
    }
    @PatchMapping("/change-order")
    public ResponseEntity<Object> changeOrder(@Valid @RequestBody BoardColumnOrderRequest columnOrderRequest, BindingResult bindingResult){
        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if(!ObjectUtils.isEmpty(errors)){
            return errors;
        }
        return columnsService.changeOrderColumn(columnOrderRequest);
    }
    @PutMapping
    public ResponseEntity<Object> updateColumn(@Valid @RequestBody BoardColumnUpdateRequest boardColumnUpdateRequest, BindingResult bindingResult){
        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if(!ObjectUtils.isEmpty(errors)){
            return errors;
        }
        return columnsService.updateColumn(boardColumnUpdateRequest);
    }
    @DeleteMapping
    public ResponseEntity<Object> deleteColumn(@Valid @RequestBody BoardColumnDeleteRequest boardColumnDeleteRequest, BindingResult bindingResult){
        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if(!ObjectUtils.isEmpty(errors)){
            return errors;
        }
        return columnsService.deleteColumn(boardColumnDeleteRequest);
    }
}
