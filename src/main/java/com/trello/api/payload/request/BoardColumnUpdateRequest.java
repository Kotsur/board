package com.trello.api.payload.request;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class BoardColumnUpdateRequest {
    @NotNull
    private Long idColumn;
    @NotEmpty
    private String name;

}
