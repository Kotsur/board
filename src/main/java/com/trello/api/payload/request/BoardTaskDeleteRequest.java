package com.trello.api.payload.request;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class BoardTaskDeleteRequest {
    @NotNull
    private Long idTask;
 }
