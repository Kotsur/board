package com.trello.api.payload.request;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class BoardTaskChangeColumnRequest {
    @NotNull
    private Long idTask;
    @NotNull
    private Long idColumn;
}
