package com.trello.api.payload.request;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class BoardColumnOrderRequest {
    @NotNull
    private Long idColumn;
    @NotNull
    @Min(0)
    private Integer orderField;
}
