package com.trello.api.payload.request;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class BoardColumnCreateRequest {
    @NotEmpty
    private String name;
}
