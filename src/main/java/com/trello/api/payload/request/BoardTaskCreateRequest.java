package com.trello.api.payload.request;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
@Data
public class BoardTaskCreateRequest {
    @NotEmpty
    private String name;
    @NotEmpty
    private String descr;
    @NotNull
    private Long idColumn;
}
