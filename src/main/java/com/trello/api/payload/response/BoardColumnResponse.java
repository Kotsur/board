package com.trello.api.payload.response;

import lombok.Data;

import java.util.List;
@Data
public class BoardColumnResponse {
    public BoardColumnResponse(Long id, String name, Integer orderField, List<BoardTaskResponse> tasksList) {
        this.id = id;
        this.name = name;
        this.orderField = orderField;
        this.tasksList = tasksList;
    }
    private Long id;
    private String name;
    private Integer orderField;
    private List<BoardTaskResponse> tasksList;
}
