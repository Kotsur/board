package com.trello.api.payload.response;

import lombok.Data;

@Data
public class BoardColmnDeleteResponse {
    public BoardColmnDeleteResponse(Long idColumn, String name) {
        this.idColumn = idColumn;
        this.name = name;
    }

    private Long idColumn;
    private String name;
}
