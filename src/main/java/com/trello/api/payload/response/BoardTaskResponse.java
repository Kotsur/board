package com.trello.api.payload.response;

import lombok.Data;

import java.time.LocalDateTime;
@Data
public class BoardTaskResponse {
    public BoardTaskResponse(Long id, LocalDateTime localDateTime, String name, String descr, Integer orderField, Long idColumn) {
        this.id = id;
        this.localDateTime = localDateTime;
        this.name = name;
        this.descr = descr;
        this.orderField = orderField;
        this.idColumn = idColumn;
    }

    private Long id;
    private LocalDateTime localDateTime;
    private String name;
    private String descr;
    private Integer orderField;
    private Long idColumn;
}
