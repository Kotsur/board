package com.trello.api.payload.response;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class ErrorTaskResponse {
    private Map<String, String> errorMap = new HashMap<>();
    private Exception boardColumnExeption;

    public ErrorTaskResponse(Exception boardColumnExeption) {
        this.boardColumnExeption = boardColumnExeption;
        errorMap.put("error", boardColumnExeption.getMessage());
    }
}
