package com.trello.api.payload.response;

import lombok.Data;

@Data
public class BoardTaskDeleteResponse {
    public BoardTaskDeleteResponse(Long idTask, String name) {
        this.idTask = idTask;
        this.name = name;
    }

    private Long idTask;
    private String name;
}
