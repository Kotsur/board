package com.trello.api.payload.response;

import com.trello.api.exeption.BoardColumnException;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class ErrorBoardColumnResponse {
    private Map<String, String> errorMap = new HashMap<>();
    private BoardColumnException boardColumnException;

    public ErrorBoardColumnResponse(BoardColumnException boardColumnExeption) {
        this.boardColumnException = boardColumnExeption;
        errorMap.put("error", boardColumnExeption.getMessage());
    }
}
