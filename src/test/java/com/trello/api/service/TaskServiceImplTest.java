package com.trello.api.service;

import com.trello.api.exeption.BoardColumnException;
import com.trello.api.exeption.BoardTaskException;
import com.trello.api.model.BoardColumn;
import com.trello.api.model.BoardTask;
import com.trello.api.payload.request.*;
import com.trello.api.repository.BoardColumnRepository;
import com.trello.api.repository.BoardTaskRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class TaskServiceImplTest {

    @Mock
    private BoardTaskRepository boardTaskRepository;

    @Mock
    private BoardColumnRepository boardColumnRepository;

    @InjectMocks
    private TaskServiceImpl taskService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }
    @Test
    public void testCreateTaskSuccess() {
        BoardColumn boardColumn = new BoardColumn();
        boardColumn.setId(1L);
        BoardTaskCreateRequest taskRequest = new BoardTaskCreateRequest();
        taskRequest.setIdColumn(1L);
        taskRequest.setName("Test Task");
        taskRequest.setDescr("Test Description");
        when(boardColumnRepository.findById(1L)).thenReturn(Optional.of(boardColumn));
        when(boardTaskRepository.findMaxOrder(boardColumn.getId())).thenReturn(0); // Assuming no tasks in the column
        when(boardTaskRepository.save(any())).thenReturn(new BoardTask());
        ResponseEntity<Object> response = taskService.createTask(taskRequest);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(boardTaskRepository, times(1)).save(any());
    }

    @Test
    public void testCreateTaskColumnNotFound() {
        BoardTaskCreateRequest taskRequest = new BoardTaskCreateRequest();
        taskRequest.setIdColumn(1L);
        when(boardColumnRepository.findById(1L)).thenReturn(Optional.empty());
        ResponseEntity<Object> response = taskService.createTask(taskRequest);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        verify(boardTaskRepository, never()).save(any());
    }
    @Test
    public void testCreateTaskException() {
        BoardColumn boardColumn = new BoardColumn();
        boardColumn.setId(1L);
        BoardTaskCreateRequest taskRequest = new BoardTaskCreateRequest();
        taskRequest.setIdColumn(1L);
        taskRequest.setName("Test Task");
        taskRequest.setDescr("Test Description");
        when(boardColumnRepository.findById(1L)).thenReturn(Optional.of(boardColumn));
        when(boardTaskRepository.findMaxOrder(boardColumn.getId())).thenReturn(0); // Assuming no tasks in the column
        when(boardTaskRepository.save(any())).thenThrow(new BoardTaskException("Error save task"));
        ResponseEntity<Object> response = taskService.createTask(taskRequest);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }
    @Test
    public void testUpdateTaskSuccess() {
        // Arrange
        BoardTaskUpdateRequest taskRequest = new BoardTaskUpdateRequest();
        taskRequest.setIdTask(1L);
        taskRequest.setName("Updated Task");
        taskRequest.setDescr("Updated Description");
        BoardTask existingTask = new BoardTask();
        existingTask.setId(1L);
        existingTask.setName("Old Task");
        existingTask.setDescr("Old Description");
        existingTask.setBoardColumn(new BoardColumn(1L, "test column", 1, Collections.emptyList()));
        when(boardTaskRepository.findById(1L)).thenReturn(Optional.of(existingTask));
        ResponseEntity<Object> response = taskService.updateTask(taskRequest);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }


    @Test
    public void testUpdateTaskNotFound() {
        BoardTaskUpdateRequest taskRequest = new BoardTaskUpdateRequest();
        taskRequest.setIdTask(1L);
        when(boardTaskRepository.findById(1L)).thenReturn(Optional.empty());
        ResponseEntity<Object> response = taskService.updateTask(taskRequest);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testDeleteTaskSuccess() {
        BoardTaskDeleteRequest deleteRequest = new BoardTaskDeleteRequest();
        deleteRequest.setIdTask(1L);
        BoardTask existingTask = new BoardTask();
        existingTask.setId(1L);
        existingTask.setName("Test Task");
        when(boardTaskRepository.findById(1L)).thenReturn(Optional.of(existingTask));
        ResponseEntity<Object> response = taskService.deleteTask(deleteRequest);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
    @Test
    public void testDeleteTaskNotFound() {
        BoardTaskDeleteRequest deleteRequest = new BoardTaskDeleteRequest();
        deleteRequest.setIdTask(1L);
        when(boardTaskRepository.findById(1L)).thenReturn(Optional.empty());
        ResponseEntity<Object> response = taskService.deleteTask(deleteRequest);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testChangeColumnTaskSuccess() {
        BoardTaskChangeColumnRequest changeColumnRequest = new BoardTaskChangeColumnRequest();
        changeColumnRequest.setIdTask(1L);
        changeColumnRequest.setIdColumn(2L);
        BoardColumn newColumn = new BoardColumn();
        newColumn.setId(2L);
        BoardTask existingTask = new BoardTask();
        existingTask.setId(1L);
        existingTask.setName("Test Task");
        when(boardColumnRepository.findById(2L)).thenReturn(Optional.of(newColumn));
        when(boardTaskRepository.findById(1L)).thenReturn(Optional.of(existingTask));
        when(boardTaskRepository.findMaxOrder(newColumn.getId())).thenReturn(0); // Assuming no tasks in the new column
        ResponseEntity<Object> response = taskService.changeColumnTask(changeColumnRequest);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testChangeColumnTaskColumnNotFound() {
        BoardTaskChangeColumnRequest changeColumnRequest = new BoardTaskChangeColumnRequest();
        changeColumnRequest.setIdTask(1L);
        changeColumnRequest.setIdColumn(2L);
        when(boardColumnRepository.findById(2L)).thenReturn(Optional.empty());
        ResponseEntity<Object> response = taskService.changeColumnTask(changeColumnRequest);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }
    @Test
    public void testChangeColumnTaskTaskNotFound() {
        BoardTaskChangeColumnRequest changeColumnRequest = new BoardTaskChangeColumnRequest();
        changeColumnRequest.setIdTask(1L);
        changeColumnRequest.setIdColumn(2L);
        when(boardColumnRepository.findById(2L)).thenReturn(Optional.of(new BoardColumn()));
        when(boardTaskRepository.findById(1L)).thenReturn(Optional.empty());
        ResponseEntity<Object> response = taskService.changeColumnTask(changeColumnRequest);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }
    @Test
    public void testChangeColumnTaskColumnException() {
        BoardTaskChangeColumnRequest changeColumnRequest = new BoardTaskChangeColumnRequest();
        changeColumnRequest.setIdTask(1L);
        changeColumnRequest.setIdColumn(2L);
        when(boardColumnRepository.findById(2L)).thenThrow(new BoardColumnException("Column not found"));
        ResponseEntity<Object> response = taskService.changeColumnTask(changeColumnRequest);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testChangeOrderTaskSuccess() {
        BoardTaskOrderRequest orderRequest = new BoardTaskOrderRequest();
        orderRequest.setIdTask(1L);
        BoardColumn column = new BoardColumn();
        column.setId(1L);
        BoardTask existingTask = new BoardTask();
        existingTask.setId(1L);
        existingTask.setName("Test Task");
        existingTask.setBoardColumn(new BoardColumn(1L, "test column", 1, Collections.emptyList()));
        when(boardTaskRepository.findById(1L)).thenReturn(Optional.of(existingTask));
        ResponseEntity<Object> response = taskService.changeOrderTask(orderRequest);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
    @Test
    public void testChangeOrderTaskTaskNotFound() {
        BoardTaskOrderRequest orderRequest = new BoardTaskOrderRequest();
        orderRequest.setIdTask(1L);
        orderRequest.setOrderField(2);
        when(boardTaskRepository.findById(1L)).thenReturn(Optional.empty());
        ResponseEntity<Object> response = taskService.changeOrderTask(orderRequest);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }
}