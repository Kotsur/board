package com.trello.api.service;

import com.trello.api.exeption.BoardColumnException;
import com.trello.api.model.BoardColumn;
import com.trello.api.payload.request.BoardColumnCreateRequest;
import com.trello.api.payload.request.BoardColumnDeleteRequest;
import com.trello.api.payload.request.BoardColumnOrderRequest;
import com.trello.api.payload.request.BoardColumnUpdateRequest;
import com.trello.api.payload.response.BoardColumnResponse;
import com.trello.api.payload.response.ErrorBoardColumnResponse;
import com.trello.api.repository.BoardColumnRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

class ColumnsServiceImplTest {

    @Mock
    private  BoardColumnRepository boardColumnRepository;
    @InjectMocks
    private ColumnsServiceImpl columnsService;
    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    void testGetAllColumns() {
        List<BoardColumn> mockBoardColumnList = Arrays.asList(
                new BoardColumn(1L, "Column 1", 1, Collections.emptyList()),
                new BoardColumn(2L, "Column 2", 2, Collections.emptyList())
        );
        when(boardColumnRepository.findAllByOrderByOrderFieldAsc()).thenReturn(mockBoardColumnList);
        ResponseEntity<List<BoardColumnResponse>> responseEntity = columnsService.getAllColumns();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        List<BoardColumnResponse> actualResponses = responseEntity.getBody();
        assertNotNull(actualResponses);
        assertEquals(mockBoardColumnList.size(), actualResponses.size());
    }



    @Test
    void testCreateColumnSuccess() {
        String columnName = "Test Column";
        BoardColumnCreateRequest createRequest = new BoardColumnCreateRequest();
        createRequest.setName(columnName);
        BoardColumn createdColumn = new BoardColumn(columnName, 1);
        when(boardColumnRepository.save(any(BoardColumn.class))).thenReturn(createdColumn);
        ResponseEntity<Object> responseEntity = columnsService.createColumn(createRequest);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        BoardColumnResponse columnResponse = (BoardColumnResponse) responseEntity.getBody();
        assertEquals(createdColumn.getId(), columnResponse.getId());
        assertEquals(createdColumn.getName(), columnResponse.getName());
        assertEquals(createdColumn.getOrderField(), columnResponse.getOrderField());
    }

    @Test
    void testCreateColumnFailure() {
        String columnName = "Test Column";
        BoardColumnCreateRequest createRequest = new BoardColumnCreateRequest();
        createRequest.setName(columnName);
        when(boardColumnRepository.save(any(BoardColumn.class))).thenThrow(new RuntimeException("Error saving column"));
        ResponseEntity<Object> responseEntity = columnsService.createColumn(createRequest);
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        ErrorBoardColumnResponse errorResponse = new ErrorBoardColumnResponse(new BoardColumnException("Error save column"));
        assertEquals(responseEntity.getBody(), errorResponse.getErrorMap());
    }



    @Test
    void testUpdateColumn_ColumnNotFound() {
        BoardColumnUpdateRequest updateRequest = new BoardColumnUpdateRequest();
        updateRequest.setIdColumn(1L);
        updateRequest.setName("UpdatedColumn");
        when(boardColumnRepository.findByIdFetchTasks(updateRequest.getIdColumn())).thenReturn(Optional.empty());
        ResponseEntity<Object> responseEntity = columnsService.updateColumn(updateRequest);
        verify(boardColumnRepository, times(1)).findByIdFetchTasks(updateRequest.getIdColumn());
        verify(boardColumnRepository, never()).save(any(BoardColumn.class));
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        ErrorBoardColumnResponse errorResponse = new ErrorBoardColumnResponse(new BoardColumnException("Column not found"));
        assertEquals(responseEntity.getBody(), errorResponse.getErrorMap());
    }


    @Test
    void testChangeOrderColumn_Success() {
         BoardColumnOrderRequest orderRequest = mock(BoardColumnOrderRequest.class);
        BoardColumn boardColumn = mock(BoardColumn.class);
        when(boardColumnRepository.findByIdFetchTasks(anyLong())).thenReturn(Optional.of(boardColumn));
        ResponseEntity<Object> result = columnsService.changeOrderColumn(orderRequest);
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }
    @Test
    void testChangeOrderColumn_Failure() {
        BoardColumnOrderRequest orderRequest = mock(BoardColumnOrderRequest.class);
        when(boardColumnRepository.findByIdFetchTasks(anyLong())).thenReturn(Optional.empty());
        ResponseEntity<Object> result = columnsService.changeOrderColumn(orderRequest);
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
        ErrorBoardColumnResponse errorResponse = new ErrorBoardColumnResponse(new BoardColumnException("Column not found"));
        assertEquals(result.getBody(), errorResponse.getErrorMap());
    }

    @Test
    void testDeleteColumn_Success() {
        BoardColumnDeleteRequest deleteRequest = mock(BoardColumnDeleteRequest.class);
        BoardColumn boardColumn = mock(BoardColumn.class);
        when(boardColumnRepository.findByIdFetchTasks(anyLong())).thenReturn(Optional.of(boardColumn));
        ResponseEntity<Object> result = columnsService.deleteColumn(deleteRequest);
        assertEquals(HttpStatus.OK, result.getStatusCode());

    }
    @Test
    void testDeleteColumn_ColumnNotFound() {
        BoardColumnDeleteRequest deleteRequest = mock(BoardColumnDeleteRequest.class);
        when(boardColumnRepository.findByIdFetchTasks(anyLong())).thenReturn(Optional.empty());
        ResponseEntity<Object> result = columnsService.deleteColumn(deleteRequest);
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
    }
}